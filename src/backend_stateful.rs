#[derive(Debug, Clone, Copy)]
pub enum ColorSpace {
    DeviceGray,
    DeviceRGB,
    DeviceCMYK,
    Pattern,
    Unimplemented,
}

#[derive(Debug, Clone)]
pub enum Color {
    Grayscale(f64),
    Rgb(f64, f64, f64),
    Unimplemented,
}

#[derive(Debug, Clone)]
pub enum StrokeJoin {
    Miter,
    Round,
    Bevel,
}

#[derive(Debug)]
struct StatefulBackend<'a> {
    plotter: &'a mut dyn Plotter,
    s: GraphicsState,
    state_stack: Vec<GraphicsState>,
}

#[derive(Debug, Clone)]
struct TransformMat {
    a: f64,
    b: f64,
    c: f64,
    d: f64,
    tx: f64,
    ty: f64,
}

#[derive(Debug, Clone)]
/// pos, path_start, and all other stored coordinates are in GLOBAL
/// coordinate space. Get from user parameters to this space with
/// transform(..) and back from global space to user space with
/// inv_transform(..)
struct GraphicsState {
    font: f64, // we only actually care about size
    pos: (f64, f64),
    path_start: (f64, f64),
    color_space: ColorSpace,
    color: Color,
    line_join: StrokeJoin,
    line_cap: StrokeJoin,
    flat: f64 ,
    transfer_function: EpsProcedure,
    path: Vec<PathSegment>,
    path_closed: bool,
    cdt: TransformMat,
}

impl<'a> StatefulBackend<'a>  {
    fn new(p: &'a mut dyn Plotter) -> Self {
        StatefulBackend {
            plotter: p,
            s: GraphicsState {
                font: 0.0,
                pos: (0.0, 0.0),
                path_start: (0.0, 0.0),
                color_space: ColorSpace::DeviceGray,
                color: Color::Grayscale(0.0),
                flat: 1.0,
                line_cap: StrokeJoin::Miter,
                line_join: StrokeJoin::Miter,
                transfer_function: EpsProcedure::Nop(0),
                path: vec![],
                path_closed: false,
                cdt: TransformMat{
                    a: 1.0,
                    b: 0.0,
                    c: 0.0,
                    d: 1.0,
                    tx: 0.0,
                    ty: 0.0,
                },
            },
            state_stack: vec![],
        }
    }
    fn transform(&self, x: f64, y:f64) -> (f64, f64) {
        self.transformt(&(x, y))
    }
    fn transformt(&self, i: &(f64, f64)) -> (f64, f64) {
        let (x, y) = i;
        (
            x*self.s.cdt.a + y*self.s.cdt.c + self.s.cdt.tx,
            x*self.s.cdt.b + y*self.s.cdt.d + self.s.cdt.ty,
            )
    }
    fn inv_transform(&self, i: (f64, f64)) -> (f64, f64) {
        let (x, y) = i;
        let mat = &self.s.cdt;
        let det = 1.0/(mat.a*mat.d - mat.b*mat.c);
        (
            det*(x*mat.d - y*mat.b - mat.tx*mat.d + mat.ty*mat.b),
            det*(-x*mat.c + y*mat.a + mat.tx*mat.c - mat.ty*mat.a),
            )
    }
}

impl<'a> GraphicsBackend for StatefulBackend<'a> {
    fn findfont(&mut self, name: String) -> FontHandle {
        eprintln!("Ignoring unimplemented function findfont");
        println!("Finding font: {}", name);
        return 1.0;
    }
    fn setdash(&mut self, offset: f64, arg: Vec<f64>) {
        eprintln!("Ignoring unimplemented function setdash");
        println!("setting dash: {}\t {:?}", offset,  arg);
    }
    fn setflat(&mut self, tol: f64) {
        println!("flattness tolerance: {}", tol);
    }
    fn setlinejoin(&mut self, tol: StrokeJoin) {
        eprintln!("Ignoring unimplemented function setflat");
        println!("Line join: {:?}", tol);
    }
    fn setlinecap(&mut self, cap: StrokeJoin) {
        println!("Line cap: {:?}", cap);
    }
    fn setmiterlimit(&mut self, limit: f64) {
        println!("miter limit: {}", limit);
    }
    fn setlinewidth(&mut self, width: f64) {
        println!("line width: {}", width);
    }
    fn setoverprint(&mut self, overprint: bool) {
        println!("Setting overprint: {}", overprint);
    }
    fn setfont(&mut self, f: FontHandle) {
        self.s.font = f;
    }
    fn translate(&mut self, x: f64, y: f64) {
        self.s.cdt.tx += x;
        self.s.cdt.ty += y;
    }
    fn moveto(&mut self, x: f64, y: f64) {
        self.s.pos = self.transform(x, y);
        if !self.s.path.is_empty() {
            eprintln!("Moveto in middle of path??");
            self.s.path.clear();
        } else {
            self.s.path_start = self.s.pos;
        }
    }
    fn settransfer(&mut self, func: EpsProcedure) {
        self.s.transfer_function = func;
    }
    fn setcolorspace(&mut self, space: ColorSpace) {
        self.s.color_space = space;
    }
    fn current_color_space(&mut self) -> ColorSpace {
        self.s.color_space
    }
    fn setcolor(&mut self, color: Color) {
        self.s.color = color;
    }
    fn rlineto(&mut self, dx: f64, dy: f64) {
        let (cx, cy) = self.s.pos;
        let (nx, ny) = self.transform(dx, dy);
        self.s.path.push(PathSegment::Line(
                (cx, cy), 
                (cx + nx, cy + ny)));
        self.s.pos = (cx + nx, cy + ny);
    }
    fn closepath(&mut self) {
        if self.s.pos != self.s.path_start {
            self.s.path.push(PathSegment::Line(
                    self.s.pos, 
                    self.s.path_start
                    ));
        }
        self.s.path_closed = true;
    }
    fn clip(&mut self) {
        self.plotter.clip_path(&self.s.path);
    }
    fn newpath(&mut self) {
        self.s.path.clear();
    }
    fn gsave(&mut self) {
        eprintln!("NOTE: the invoked gsave should clear the clipping path.{}",
                  "\nThis implementation pretty much ignores those.");
        self.state_stack.push(self.s.clone());
    }
    fn concat(&mut self, v: Vec<f64>) {
        assert!(v.len() == 6, "Matrix inputs must be exactly 6 entries");
        let o = &self.s.cdt;
        let n = TransformMat {
            a: o.a*v[0] + o.b*v[2],
            b: o.a*v[1] + o.b*v[3],
            c: o.c*v[0] + o.d*v[2],
            d: o.c*v[1] + o.d*v[3],
            tx:o.tx*v[0]+ o.ty*v[2] + v[4],
            ty:o.tx*v[1]+ o.ty*v[3] + v[5],
        };
        self.s.cdt = n;
    }
    fn lineto(&mut self, x: f64, y: f64) {
        let (cx, cy) = self.s.pos;
        let (tx, ty) = self.transform(x, y);
        self.s.path.push(PathSegment::Line(
                (cx, cy), 
                (tx, ty)));
        self.s.pos = (tx, ty);
    }
    fn stroke(&mut self) {
        if self.s.path_closed {
            self.plotter.closed_shape(&self.s.path);
        } else {
            self.plotter.open_path(&self.s.path);
        }
    }
    fn grestore(&mut self) {
        eprintln!("NOTE: the invoked grestore should set the clipping path{}",
                  "\nbut this implementation pretty much ignores those.");
        self.s = match self.state_stack.pop() {
            Some(t) => t,
            None => return (), // spec says to silently ignore
        }
    }
    fn showpage(&mut self) {
        self.plotter.page();
    }
    fn scale(&mut self, x: f64, y: f64) {
        self.concat(vec![x, 0.0, 0.0, y, 0.0, 0.0]);
    }
    fn currentmatrix(&mut self) -> [f64; 6] {
        let m = &self.s.cdt;
        [m.a, m.b, m.c, m.d, m.tx, m.ty]
    }
    fn setmatrix(&mut self, new: [f64; 6]) -> [f64; 6] {
        let m = &mut self.s.cdt;
        m.a = new[0];
        m.b = new[1];
        m.c = new[2];
        m.d = new[3];
        m.tx = new[4];
        m.ty = new[5];
        new
    }
    fn set_page_size(&mut self, w: f64, h: f64) {
        self.plotter.set_page_size(w, h);
    }
    fn currentpoint(&mut self) -> (f64, f64) {
        self.inv_transform(self.s.pos)
    }
}
