/// Reads bytes from a buffer until a lambda returns true
/// 
/// Compare `BufRead.read_until`, which does the same thing except
/// it stops at a given byte terminator, instead of an arbitrary
/// state.
///
/// Results are placed into `out`; `out` is NOT automatically cleared.
///
/// The returned `u8` is the byte that the lambda decided to stop on.
/// This character is NOT copied to `out`
/// Errors are passed on from `BufRead.fill_buf`.
///
/// If the end of the file is reached, whatever was gathered is left in
/// `out` and a `None` is returned.
///
/// The lambda must return (hit-end-of-token, consume-this-byte).
/// consume-this-byte only respected if at end of token.
fn buf_read_until<T, Z>
(state: &mut Z, bufr: &mut T, stop: &'static dyn Fn(&mut Z, u8)->(bool, bool),
out: &mut Vec<u8>)
-> Result<Option<u8>, Error>
where T: BufRead
{
    let mut i = 0;
    let mut consume = true;
    let (res, done) = {
        let buf = bufr.fill_buf()?;
        if buf.len() == 0 {
            (Ok(None), true)
        } else {
            loop {
                let (next, t) = (*stop)(state, buf[i]);
                consume = t;
                if !next || consume {
                    out.push(buf[i]);
                }
                if next {
                    break (Ok(Some(buf[i])), true);
                }                i += 1;
                if i >= buf.len() {
                    break (Ok(None), false);
                }
            }
        }
    };
    if consume {
        bufr.consume(i+1);
    } else {
        bufr.consume(i);
    }
    if done {
        return res;
    } else {
        return buf_read_until(state, bufr, stop, out);
    }
}
