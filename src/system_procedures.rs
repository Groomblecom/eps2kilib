
fn get_numeric(x: &mut EpsState) -> f64 {
    let a = x.vm.stack.pop().expect("Operating on empty stack!");
    if let EpsValue::Num(c)=a {
        return c 
    } else {
        panic!("Numeric operator must have numeric inputs, got {:?}\n{:?}",
               a, x);

    }
}

fn get_int(x: &mut EpsState) -> u64 {
    let int = get_numeric(x);
    assert_eq!(int, int.trunc(), 
               "Operator requires integer size!"
               );
    int as u64
}

fn get_sint(x: &mut EpsState) -> i64 {
    let int = get_numeric(x);
    assert_eq!(int, int.trunc(), 
               "Operator requires integer size!"
               );
    int as i64
}

fn get_name(x: &mut EpsState) -> String {
    let a = x.vm.stack.pop().expect("Operating on empty stack!");
    if let EpsValue::Name(c)=a {
        return c 
    } else {
        panic!("Operator must have name input, got {:?}\n{:?}", a,x);
    }
}

fn get_font(x: &mut EpsState) -> f64 {
    let a = x.vm.stack.pop().expect("Operating on empty stack!");
    if let EpsValue::Font(c)=a {
        return c 
    } else {
        panic!("font operator must have font input, got {:?}\n{:?}", a,x);
    }
}

fn get_string(x: &mut EpsState) -> Rc<RefCell<String>> {
    let a = x.vm.stack.pop().expect("Operating on empty stack!");
    if let EpsValue::String(s)=a {
        return s; 
    } else {
        panic!("string operator must have string input, got {:?}\n{:?}", a,x);
    }
}

fn get_dict(x: &mut EpsState) -> Rc<RefCell<EpsDict>> { 
    let a = x.vm.stack.pop().expect("Operating on empty stack!");
    if let EpsValue::Dict(dict)=a {
        return dict
    } else {
        panic!("Dictionary operator must have dict inputs, got {:?}\n{:?}",
               a, x);
    }
}

fn get_arr(x: &mut EpsState) -> Rc<ArrayCell> { 
    let a = x.vm.stack.pop().expect("Operating on empty stack!");
    if let EpsValue::Array(arr)=a {
        return arr 
    } else {
        panic!("Array operator must have array inputs, got {:?}\n{:?}",
               a, x);
    }
}

/// Returned array does not reflect backing store of value.
fn get_num_arr(x: &mut EpsState) -> Vec<f64> { 
    let a = get_arr(x);
    let mut nvec = vec![];
    for elt in (*a).borrow().iter() {
        if let EpsValue::Num(f) = elt {
            nvec.push(*f);
        } else {
            panic!("Numeric array operator {}, got {:?}\n{:?}",
                   "must have numeric input",
                   elt, x);
        }
    }
    return nvec;
}

/// Returned array does not reflect backing store of value.
fn get_graphics_matrix(x: &mut EpsState) -> [f64; 6] { 
    let b = get_arr(x);
    let a = (*b).borrow();
    assert_eq!(a.len(), 6, "Graphics transform matricies must have \
    exactly 6 elements");
    let mut output = [0.0; 6];
    for i in 0..6 {
        if let EpsValue::Num(n) = a[i] {
            output[i] = n;
        } else {
            panic!("Graphics matricies may only have number elements");
        }
    }
    output
}

fn get_bool(x: &mut EpsState) -> bool {
    let a = x.vm.stack.pop().expect("Operating on empty stack!");
    if let EpsValue::Bool(c)=a {
        return c 
    } else {
        panic!("Boolean operator must have bool inputs, got {:?}\n{:?}",
               a, x);

    }
}

fn get_proc(x: &mut EpsState) -> EpsProcedure {
    let a = x.vm.stack.pop().expect("Operating on empty stack!");
    if let EpsValue::Procedure(c)=a {
        return c 
    } else {
        panic!("{}, got {:?}\n{:?}",
               "Flow control operator must have procedure inputs",
               a, x);

    }
}

fn get_stroke_join(x: &mut EpsState) -> StrokeJoin {
    let a = get_int(x);
    match a {
        0 => StrokeJoin::Miter,
        1 => StrokeJoin::Round,
        2 => StrokeJoin::Bevel,
        _ => {
            panic!("Stroke join styles are in range 0..2!, got {}\n{:?}", a, x);
        }
    }
}

fn get_color_space(x: &mut EpsState) -> ColorSpace {
    let a = x.vm.stack.pop().expect("Operating on empty stack!");
    let s = match a {
        EpsValue::Name(s) => s,
        EpsValue::Array(_) => {
            panic!("Parametrized colorspaces not supported! \n {:?}", x);
        },
        _ => {
            panic!("Colorspace operator {}, got {:?}\n{:?}",
                   "must have array or ident input", a,x);
        },
    };
    match s.as_str() {
        "DeviceGray" => ColorSpace::DeviceGray,
        "DeviceRGB" => ColorSpace::DeviceRGB,
        "DeviceCMYK" => ColorSpace::DeviceCMYK,
        "Pattern" => ColorSpace::Pattern,
        _ => {
            eprint!("Invalid or unsupported color space name, got {}\n{:?}", s, x);
            ColorSpace::Unimplemented
        },
    }
}

fn get_eps_value(x: &mut EpsState) -> EpsValue {
    x.vm.stack.pop().expect("Operating on empty stack!")
}

include!{"operator_wrapper_macros.rs"}
type Name = String;
type FontHandle = f64;
type Num = f64;
type Bool = bool;
type NumArray = Vec<f64>;
type GMatrix = [f64; 6];

generate_eps_graphics_glue!{
    inject_graphics_operators
    trait GraphicsBackend: Debug {
        fn findfont(&mut self, name: Name) -> FontHandle;
        fn setdash(&mut self, offset: f64, arg: NumArray);
        fn setflat(&mut self, tolerance: f64);
        fn setlinejoin(&mut self, join: StrokeJoin);
        fn setlinecap(&mut self, cap: StrokeJoin);
        fn setmiterlimit(&mut self, limit: f64);
        fn setlinewidth(&mut self, width: f64);
        fn setoverprint(&mut self, overprint: Bool);
        fn setfont(&mut self, f: FontHandle);
        fn translate(&mut self, x: f64, y: f64);
        fn moveto(&mut self, x: f64, y: f64);
        fn settransfer(&mut self, func: EpsProcedure);
        fn setcolorspace(&mut self, space: ColorSpace);
        // Internal-only "hidden" function/instruction:
        fn r#current_color_space(&mut self) -> ColorSpace;
        fn setcolor(&mut self, _color: Color);
        fn rlineto(&mut self, dx: f64, dy: f64);
        fn closepath(&mut self);
        fn clip(&mut self);
        fn newpath(&mut self);
        fn gsave(&mut self);
        fn concat(&mut self, arg: NumArray);
        fn lineto(&mut self, x: f64, y: f64);
        fn stroke(&mut self);
        fn grestore(&mut self);
        fn showpage(&mut self);
        // Technically, scale is variadic and can take a matrix as arg.
        // Ignored here; will add if necessary.
        fn scale(&mut self, x: f64, y: f64);
        fn currentmatrix(&mut self) -> GMatrix;
        fn setmatrix(&mut self, m: GMatrix) -> GMatrix;
        // Internal-only "hidden" function/instruction:
        fn r#set_page_size(&mut self, w: f64, h: f64);
        // Will be overridden with manually-injected function
        fn currentpoint(&mut self) -> (f64, f64);
    }
}

type Any = EpsValue;
type EString = Rc<RefCell<String>>;
type Array = Rc<ArrayCell>;

// This macro inverts the order of returned values from
// the official LRM notation!
generate_eps_glue!{
    inject
    mod base_operators {
        fn pop(_discard: Any) -> () {}
        fn add(a: Num, b:Num) -> (Num) { (a+b) }
        fn sub(a: Num, b:Num) -> (Num) { (a-b) }
        fn mul(a: Num, b:Num) -> (Num) { (a*b) }
        fn div(a: Num, b:Num) -> (Num) { (a/b) }
        fn idiv(a: Num, b:Num) -> (Num) { (f64::trunc(a/b)) }
        fn r#mod(a: Num, b:Num) -> (Num) { (a%b) }
        fn neg(a: Num) -> (Num) { (-a) }
        fn round(a: Num) -> (Num) { a.round() }
        fn abs(a: Num) -> (Num) { a.abs() }
        fn r#true() -> (Bool) { (true) }
        fn r#false() -> (Bool) { (false) }
        // Supposed to also take strings to compare but whatever
        fn lt(a: Num, b: Num) -> (Bool) { b < a }
        fn not(a: Bool) -> (Bool) { (!a) }
        fn exch(a: Any, b: Any) -> (Any, Any) { (a, b) }
        fn dup(a: Any) -> (Any, Any) { (a.clone(), a) }
        fn cvn(a: EString) -> (Name) {
            (*a).borrow().clone()
        }
        fn eq(a: Any, b: Any) -> (Bool) {
            a == b
        }
        fn matrix() -> (Array) {
            let one = crate::EpsValue::Num(1.0);
            let zero = crate::EpsValue::Num(0.0);
            let out = vec![one.clone(), zero.clone(), zero.clone(), 
            one.clone(), zero.clone(), zero.clone()];
            std::rc::Rc::new(std::cell::RefCell::new(out))
        }
    }
}


fn inject_procedure(func: &'static dyn Fn(&mut EpsState),
                    name: &str, 
                    into: &mut HashMap<String, EpsValue>) {
    into.insert(name.to_string(),
    EpsValue::Procedure(EpsProcedure::Builtin(
            func, name.to_string()))
    );
}


fn get_system_dict() -> HashMap<String, EpsValue> {
    let mut map = HashMap::new();

    inject_graphics_operators(&mut map);

    // Base operators override graphics operators. This enables
    // variadic wrappers of non-variadic graphics functions.
    base_operators::inject(&mut map);

    inject_procedure(
        &|x: &mut EpsState| {
            let len = get_int(x);
            x.vm.stack.push(EpsValue::Dict(Rc::new(RefCell::new(EpsDict {
                vals: HashMap::new(),
                size: len as usize,
                global: false,
            }))));
        }, "dict", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            let a = x.vm.stack.pop().expect("Operating on empty stack!");
            let b = get_name(x);
            (*(**x.vm.dict_stack.last().expect("there should always be a dict"))
                .borrow_mut()).vals.insert(b, a);
        }, "def", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            let a = get_dict(x);
            x.vm.dict_stack.push(a);
        }, "begin", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            x.vm.dict_stack.pop().expect("Cannot pop empty dict stack!");
        }, "end", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            let name = get_name(x);
            for dict in x.vm.dict_stack.iter().rev() {
                if let Some(_v) = (*dict).borrow().vals.get(&name) {
                    x.vm.stack.push(EpsValue::Dict(Rc::clone(dict)));
                    x.vm.stack.push(EpsValue::Bool(true));
                    return;
                }
            }
            x.vm.stack.push(EpsValue::Bool(false));
        }, "where", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            let second = get_proc(x);
            let first = get_proc(x);
            let takefirst = get_bool(x);
            if takefirst {
                execute_procedure(first, x);
            } else {
                execute_procedure(second, x);
            }
        }, "ifelse", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            let len = get_int(x);
            x.vm.stack.push(EpsValue::String(Rc::new(RefCell::new(
                        " ".repeat(len as usize)
                        ))));
        }, "string", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            let first = get_proc(x);
            let takefirst = get_bool(x);
            if takefirst {
                execute_procedure(first, x);
            }
        }, "if", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            let arr = get_arr(x);
            for elt in (*arr).borrow().iter() {
                x.vm.stack.push(elt.clone());
            }
            x.vm.stack.push(EpsValue::Array(arr));
        }, "aload", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            let a = get_numeric(x);
            let font = get_font(x);
            x.vm.stack.push(EpsValue::Font(font*a));
        }, "scalefont", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            // I bet cloning the procedure every time causes
            // some massive slowdown but the potential for 
            // self-modifying functions is a bit much
            let proc = get_proc(x);
            let compos = x.vm.stack.pop().expect("Operating on empty stack!");
            if let EpsValue::Dict(dict)=compos {
                for elt in (*dict).borrow().vals.values() {
                    x.vm.stack.push(elt.clone());
                    execute_procedure(proc.clone(), x);
                }
            } else if let EpsValue::Array(ai)=compos {
                let arr = ai.clone();
                for elt in (*arr).borrow().iter() {
                    x.vm.stack.push(elt.clone());
                    execute_procedure(proc.clone(), x);
                }
            } else {
                panic!("Forall can only be invoked {} got {:?}\n{:?}",
                       "on composite objects!", compos, x);
            }
        }, "forall", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            let o = x.vm.stack.pop().expect("Operating on empty stack!");
            println!("== {:?}", o);
        }, "==", &mut map);
    inject_procedure(
        &|s: &mut EpsState| {
            let j = get_sint(s);
            let n = get_int(s) as i64;
            assert!(-n < j && j < n, 
                "Roll operator j must be bounded  by +- n, j={} n={}\n{:?}",
                j, n, s);
            if j == 0 || j == n || j == -n {
                return;
            }
            let mut buffer = vec![];
            for _i in 0..n {
                buffer.push(s.vm.stack.pop()
                            .expect("Roll requires n values."));
            }
            if j > 0 {
                buffer.rotate_left(j as usize);
            } else {
                buffer.rotate_right((-j) as usize);
            }
            for _i in 0..n {
                s.vm.stack.push(buffer.pop().expect("impossible roll error"));
            }
        }, "roll", &mut map);
    inject_procedure(
        &|s: &mut EpsState| {
            let offset = get_int(s) as usize;
            assert!((s.vm.stack.len()-1) >= offset, 
                "Index operator requires at n stack entries. n:{}\n{:?}",
                offset,s);
            s.vm.stack.push(s.vm.stack[s.vm.stack.len()-offset-1].clone());
        }, "index", &mut map);
    // Non-variadic but requires full state access
    inject_procedure(
        &|s: &mut EpsState| {
            let r = get_proc(s);
            let linenum = match r {
                EpsProcedure::User(_, l) => l,
                EpsProcedure::Nop(l) => l,
                EpsProcedure::Builtin(_, _) => 0,
            };
            let mut toks = vec![];
            bind_procedure(r, s, &mut toks);
            s.vm.stack.push(EpsValue::Procedure(
                    EpsProcedure::User(toks.join(" "), linenum)));
        }, "bind", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            x.vm.stack.push(EpsValue::Dict(Rc::clone(&x.vm.status_dict)));
        }, "statusdict", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            x.vm.stack.push(EpsValue::Dict(Rc::clone(&x.vm.user_dict)));
        }, "userdict", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            let value = get_eps_value(x);
            let key = get_eps_value(x);
            let composite = get_eps_value(x);
            match composite {
                EpsValue::Dict(dict) => {
                    if let EpsValue::Name(k)=key {
                        let mut m = dict.borrow_mut();
                        m.vals.insert(k, value);
                    } else {
                        panic!("put on dict must have name key, got {:?}",
                               key);
                    }
                },
                EpsValue::Array(arr) => {
                    if let EpsValue::Num(k)=key {
                        let mut m = arr.borrow_mut();
                        m[k as usize] = value;
                    } else {
                        panic!("put on array must have index, got {:?}",
                               key);
                    }
                }
                _ => {
                    panic!("Impermissible composite argument to put: {:?}",
                           composite);
                }
            }
        }, "put", &mut map);
    // Not variadic nor requires state access, just an awkward name
    inject_procedure(
        &|x: &mut EpsState| {
            x.vm.stack.push(EpsValue::Mark);
        }, "[", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            let mut out = vec![];
            loop {
                let v = x.vm.stack.pop().expect(
                    "Missing ']'? emptied stack in array literal");
                if let EpsValue::Mark=v {
                    break;
                }
                out.push(v);
            }
            out.reverse();
            x.vm.stack.push(EpsValue::Array(Rc::new(RefCell::new(out))));
        }, "]", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            let key = get_name(x);
            let dict = get_dict(x);
            let res = (*dict).borrow().vals.contains_key(&key);
            x.vm.stack.push(EpsValue::Bool(res));
        }, "known", &mut map);
    inject_procedure( // TODO: not variadic -- why is this here?
        &|x: &mut EpsState| {
            let len = get_int(x);
            let mut out = vec![];
            for _i in 0..len {
                out.push(EpsValue::Null);
            }
            out.reverse();
            x.vm.stack.push(EpsValue::Array(Rc::new(RefCell::new(out))));
        }, "array", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            let proc = get_proc(x);
            let limit = get_numeric(x);
            let increment = get_numeric(x);
            let initial = get_numeric(x);
            let mut control = initial;
            while control < limit {
                x.vm.stack.push(EpsValue::Num(control));
                execute_procedure(proc.clone(), x);
                control += increment;
            }
        }, "for", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            let strparam = get_string(x);
            let radix = get_int(x);
            let num = get_numeric(x);
            assert!(radix <= 36, 
                    "ASCII strings cannot safely encode bases larger than 36");
            let ret = match radix {
                10 => num.to_string(),
                _ => {
                    // This horrifying cast necessary to handle negatives
                    // as overflowed unsigned values, per LRM. Sigh.
                    let len = (((num as i32) as u32 ) as f64)
                        .log(radix as f64).ceil() as usize;
                    let mut num = num as u32;
                    let radix = radix as u32;
                    let mut chs = Vec::with_capacity(len);
                    for _i in 0..len {
                        chs.push(std::char::from_digit(num%radix, radix)
                                 .unwrap());
                        num /= radix;
                    }
                    chs.iter().rev().collect()
                }
            };
            let mut s = strparam.borrow_mut();
            s.replace_range(0..ret.len(), &ret);
            x.vm.stack.push(EpsValue::String(Rc::new(RefCell::new(ret))));
            
        }, "cvrs", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            x.vm.stack.push(EpsValue::Save(Rc::new(x.vm.break_rc_clone())));
        }, "save", &mut map);
    inject_procedure( // Must be here because needs to delegate to graphics fn
        &|x: &mut EpsState| {
            let a = match x.vm.stack.pop() {
                Some(EpsValue::Array(ptr)) => ptr,
                _ => panic!("currentmatrix requires a matrix to fill"),
            };
            {
                let mut inner = (*a).borrow_mut();
                assert!(inner.len() == 6, 
                        "Cannot copy current transform \
                        matrix into a too-small mat"
                        );
                let smat = x.graphics_backend.currentmatrix();
                for i in 0..6 {
                    inner[i] = EpsValue::Num(smat[i]);
                }
            }
            x.vm.stack.push(EpsValue::Array(a));
        }, "currentmatrix", &mut map);
    inject_procedure( // Must be here because it's very variadic
        &|x: &mut EpsState| {
            // TODO: add support for strings and dicts, per spec
            let index = x.vm.stack.pop();
            let a = match x.vm.stack.pop() {
                Some(EpsValue::Array(ptr)) => ptr,
                _ => panic!("get operator only supports arrays at this point!"),
            };
            let b = match index {
                Some(EpsValue::Num(i)) => i,
                _ => panic!("get operator with array requires numeric index!"),
            };
            let res = {
                let inner = (*a).borrow();
                inner[b as usize].clone()
            };
            x.vm.stack.push(res);
        }, "get", &mut map);
    inject_procedure( // Must be here because it requires access to dict stack
        &|x: &mut EpsState| {
            let tokstr = get_name(x);
            let mut fproc = None;
            for dict in x.vm.dict_stack.iter().rev() {
                if let Some(v) = (*dict).borrow().vals.get(&tokstr) {
                    fproc = Some(v.clone());
                }
            }
            if let Some(proc) = fproc {
                x.vm.stack.push(proc);
            } else {
                panic!("Tried to load nonexistant value!");
            }
        }, "load", &mut map);
    // It's variadic so setcolor has to be here :(
    // TODO: move it out of here, now that above override works, see top
    inject_procedure(
        &|s: &mut EpsState| {
            let space = s.graphics_backend.current_color_space();
            let res = match space {
                ColorSpace::DeviceGray => {
                    Color::Grayscale(get_numeric(s))
                },
                ColorSpace::DeviceRGB => {
                    Color::Rgb(get_numeric(s), get_numeric(s), 
                                  get_numeric(s))
                },
                _ => {
                    panic!("Tried to set color while in{}, {:?}",
                           " unimplemented color space!", space);
                },
            };
            s.graphics_backend.setcolor(res);
        }, "setcolor", &mut map);
    inject_procedure(
        &|s: &mut EpsState| {
            let level = get_numeric(s);
            assert!(level <= 1.0 && level >= 0.0, 
                    "Gray levels must be in 1..0 interval");
            let res = Color::Grayscale(level);
            s.graphics_backend.setcolor(res);
        }, "setgray", &mut map);
    inject_procedure(
        &|x: &mut EpsState| {
            let proc = get_proc(x);
            let limit = get_int(x);
            for _i in 0..limit {
                execute_procedure(proc.clone(), x);
            }
        }, "repeat", &mut map);
    inject_procedure(
        &|s: &mut EpsState| {
            let rlevel = get_numeric(s);
            let glevel = get_numeric(s);
            let blevel = get_numeric(s);
            assert!(rlevel <= 1.0 && rlevel >= 0.0, 
                    "Red levels must be in 1..0 interval");
            assert!(glevel <= 1.0 && glevel >= 0.0, 
                    "Red levels must be in 1..0 interval");
            assert!(blevel <= 1.0 && blevel >= 0.0, 
                    "Red levels must be in 1..0 interval");
            let res = Color::Rgb(rlevel, glevel, blevel);
            s.graphics_backend.setcolor(res);
        }, "setrgbcolor", &mut map);
    inject_procedure( // Funky name.
        &|x: &mut EpsState| {
            x.vm.stack.push(EpsValue::Mark);
        }, "<<", &mut map);
    inject_procedure( // Funky name, variadic
        &|x: &mut EpsState| {
            let mut i = 0;
            let mut dict = HashMap::new();
            loop {
                let val = x.vm.stack.pop()
                    .expect("Uneven number of items in dict literal!");
                if let EpsValue::Mark = val {
                    break;
                } else  {
                    let key = x.vm.stack.pop()
                        .expect("Emptied stack in dict literal");
                    if let EpsValue::Name(s) = key {
                        dict.insert(s, val);
                    } else {
                        panic!("Dict keys must be names!");
                    }
                }
                i += 1;
            }
            let res = EpsDict {
                vals: dict,
                size: i,
                global: false,
            };
            let resval = EpsValue::Dict(Rc::new(RefCell::new(res)));
            x.vm.stack.push(resval);
        }, ">>", &mut map);
    inject_procedure( // Needs direct calls onto graphics env.
        &|x: &mut EpsState| {
            // So, this is really supposed to merge things into a special
            // dictionary, and then the graphics backend is supposed to use
            // those, somehow. For sanity sake, I'm containing the nonsense
            // here, and then just looking up special keys and passing them
            // to hidden functions in the graphics backend.
            let dict = get_dict(x);
            let dictref = (*dict).borrow();
            let size = dictref.vals.get("PageSize");
            if let Some(EpsValue::Array(arrptr)) = size {
                let arrref = (*arrptr).borrow();
                let wval = arrref.get(0).expect(
                "PageSize entry in device dict must have two entries");
                let hval = arrref.get(1).expect(
                "PageSize entry in device dict must have two entries");
                if let (EpsValue::Num(w), EpsValue::Num(h)) = (wval, hval) {
                    x.graphics_backend.set_page_size(*w, *h);
                } else {
                    panic!(
                        "PageSize WxH in device dict MUST be numbers, Got {:?}",
                        arrptr);
                }
            } else if None != size {
                panic!(
                    "PageSize entry in device dict MUST be array. Got: {:?}",
                    dict);
            } // if it's None, then whatever. Ignore it.
            eprintln!("Page device map accepted with: {:?}", dict);
        }, "setpagedevice", &mut map);
    inject_procedure( // Needs to overwrite + delegate to graphics func
        &|x: &mut EpsState| {
            let (ptx, pty) = x.graphics_backend.currentpoint();
            x.vm.stack.push(EpsValue::Num(ptx));
            x.vm.stack.push(EpsValue::Num(pty));
        }, "currentpoint", &mut map);

    map
}
