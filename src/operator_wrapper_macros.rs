
macro_rules! get_eps_arg_by_type {
    (Name, $state:ident) => {crate::get_name($state)};
    (Bool, $state:ident) => {crate::get_bool($state)};
    (NumArray, $state:ident) => {crate::get_num_arr($state)};
    (f64, $state:ident) => {crate::get_numeric($state)};
    (Num, $state:ident) => {crate::get_numeric($state)};
    (StrokeJoin, $state:ident) => {crate::get_stroke_join($state)};
    (ColorSpace, $state:ident) => {crate::get_color_space($state)};
    // Color inputs are variadic (to a degree
    // dependant on state) and so can't be read in with this
    // mechanism. This lets them get declared normally.
    (Color, $state:ident) => {unreachable!()};
    (EpsProcedure, $state:ident) => {crate::get_proc($state)};
    (FontHandle, $state:ident) => {crate::get_font($state)};
    (Any, $state:ident) => {crate::get_eps_value($state)};
    (EString, $state:ident) => {crate::get_string($state)};
    (GMatrix, $state:ident) => {crate::get_graphics_matrix($state)};
}

macro_rules! generate_eps_graphics_glue {
    ($injfname:ident
     trait $tname:ident : Debug{
        $(
            fn $fname:ident
            ( &mut self $(, $argname:ident : $argtype:ident)* )
            $( -> $rettype:ty )*
            ;
        )*
    }) => {
        pub trait $tname :Debug {
            $(
                fn $fname(&mut self $(, $argname : $argtype)*) $(-> $rettype)*;
            )*
        }

        fn $injfname (map: &mut HashMap<String, EpsValue>) {
            #![allow(unreachable_code, unused_variables)]
            $(
                inject_procedure(
                    &|state: &mut EpsState| {
                        $(
                            let $argname = get_eps_arg_by_type!( $argtype, state ); 
                          )*
                         state.graphics_backend.$fname( $($argname),* );
                    }, stringify!($fname), map);
            )*
        }
    }
}

// I am proud I wrote this and apologetic if you have to read it.
// It takes a normal rust function and wraps it in a closure that pulls
// appropriately-typed arguments off the EPS stack, runs the function with
// them, then pushes the results back onto the EPS stack, wrapped in
// appropriately-typed EpsValue instances. This whole nightmare evaluates at
// compile time; the compiler is free to go inline or whatever.
//
// This macro inverts the order of returned values from
// the official LRM notation!
macro_rules! generate_eps_glue {
    ($injfname:ident
     mod $mname:ident {
        $(
            fn $fname:ident
            ($($argname:ident : $argtype:ident),* )
            -> ( $($rettype:ident),* ) $body:block
        )*
    }) => {
        #[allow(unused_parens)]
        mod $mname {
            $(
                fn $fname($( $argname : crate::$argtype),*) -> ( $( to_rettype!($rettype) ),* ) {$body}
            )*

            pub(in crate) fn $injfname (map: &mut std::collections::HashMap<String, crate::EpsValue>) {
                $(
                    crate::inject_procedure(
                        &|state: &mut crate::EpsState| {
                            $(
                                let $argname = get_eps_arg_by_type!( 
                                    $argtype, state ); 
                              )*
                                make_retvars!(state $fname $($argname),* [] $($rettype)* );
                        }, stringify!($fname), map);
                )*

                // Inject doubles without r# prefixes (but not from graphics
                // because there it helps hide implementation-specific commands)
                let mut badkeys = vec![];
                for key in map.keys() {
                    if key.starts_with("r#") {
                        badkeys.push(key.clone());
                    }
                }
                for key in badkeys {
                    let val = map.get(&key).unwrap().clone();
                    map.insert(key[2..].to_string(), val);
                }
            }
        }
    }
}

macro_rules! make_retvars {
    ($state:tt $fname:tt $($argname:ident),* [ $( $previous:tt )* ] $temp:tt $($rest:tt)* ) => {
        make_retvars!($state $fname $($argname),* [ $($previous)* $temp x ] $($rest)*)
    };
    ($state:tt $fname:ident $($argname:ident),* [ $($temp:ident $done:tt )* ] ) => {
        let ( $($done),* ) = $fname($($argname),*);
        $(
        $state.vm.stack.push(return_eps_arg!($temp, $done));
        )*
    };
}

macro_rules! return_eps_arg {
    (Num, $var:ident) => {crate::EpsValue::Num($var)};
    (Bool, $var:ident) => {crate::EpsValue::Bool($var)};
    (Name, $var:ident) => {crate::EpsValue::Name($var)};
    (Array, $var:ident) => {crate::EpsValue::Array($var)};
    (Any, $var:ident) => {$var};
}

macro_rules! to_rettype {
    ($i:ident) => {crate::$i};
}

// Minimial version of above mess: https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=3f85dc4684829310a579c23bf3ff6bd3
