#[derive(PartialEq, Clone, Debug)]
enum ParserMode {
    Token,
    Procedure(u64), //depth
    String(Box<ParserMode>, bool), //in backslash-escaped mode
    //         ^^^^^^^^^^ Helps to deal with things like { (string } strang) }
    Comment(Box<ParserMode>), //which parsing context to return to
    Boundary,
}

#[derive(Debug, Clone)]
struct ParserState {
    mode: ParserMode,
    line_num: u64,
    context: String,
}

///
/// I have decided to quietly ignore the possibility of
///
///   - procedure literals in arrays.
///   - backslash-escape sequences other than \) and \\ in strings
///   - Probably some other edge cases, I've been working on this
///     for hours.
///
/// Returns (hit-end-of-token, consume-this-byte)
fn parser(state: &mut ParserState, c: u8) -> (bool, bool) {
    if c == b'\n' {
        state.line_num += 1;
    }
    let res = match state.mode.clone() {
        ParserMode::Boundary => {
            let (mode, done) = match c {
                b' ' | b'\n' => (ParserMode::Boundary, false),
                b'{' => (ParserMode::Procedure(1), false),
                b'(' => (ParserMode::String(Box::new(ParserMode::Boundary), false), false),
                b'%' => (ParserMode::Comment(Box::new(ParserMode::Boundary)), false),
                b'[' => (ParserMode::Boundary, true),
                b']' => (ParserMode::Boundary, true),
                _    => (ParserMode::Token, false),
            };
            state.mode = mode;
            (done, true)
        },
        ParserMode::Token => {
            match c {
                b' ' | b'\n' => (true, true),
                b'{' => {
                    state.mode = ParserMode::Procedure(1);
                    (true, false)
                },
                b'[' => {
                    state.mode = ParserMode::Token;
                    (true, false)
                },
                b']' => {
                    state.mode = ParserMode::Token;
                    (true, false)
                },
                b'/' => {
                    (true, false)
                },
                _ => (false, true),
            }
        },
        ParserMode::Comment(newmode) => {
            match c {
                b'\n' => {
                    state.mode = *newmode;
                    (state.mode == ParserMode::Boundary, true)
                },
                _ => (false, true),
            }
        },
        ParserMode::Procedure(depth) => {
            match c {
                b'%' => {
                    state.mode = ParserMode::Comment(Box::new(state.mode.clone()));
                    (false, true)
                },
                b'}' => {
                    let ndepth = depth - 1;
                    state.mode = ParserMode::Procedure(ndepth);
                    (ndepth==0, true)
                },
                b'{' => {
                    let ndepth = depth + 1;
                    state.mode = ParserMode::Procedure(ndepth);
                    (false, true)
                },
                b'(' => {
                    state.mode = ParserMode::String(Box::new(state.mode.clone()), false); 
                    (false, true)
                },
                _ => (false, true),
            }
        },
        ParserMode::String(newmode, slashed) => {
            match c {
                b')' if !slashed  => {
                    state.mode = *newmode;
                    (state.mode == ParserMode::Boundary, true)
                },
                b'\\' if !slashed  => {
                    state.mode = ParserMode::String(newmode, true);
                    (false, true)
                },
                _ => {
                    state.mode = ParserMode::String(newmode, false);
                    (false, true)
                },
            }
        },
    };
    if res.0 {
        state.mode = ParserMode::Boundary;
    }
    res
}
