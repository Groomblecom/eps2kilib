//! # Roastscript (rust-postscript)
//!
//! Sometimes, you need to read a postscript file. This library
//! implements a subset of the beautiful, turing complete postscript
//! language. It outputs things to a 'graphics backend' so you can handle
//! them in whatever manner you want.
//!
//! There are some builtin graphics backends, like the stdout backend.
//!
//! It goes without saying, but you really, really shouldn't open untrusted
//! postscript files (with any program, but especially this untested library).
//! The really dangerous stuff (e.g. file I/O) isn't implemented, but
//! PostScript is a extremely versatile stack-based programming language that
//! happens to have a really good built in graphics library. It is impossible
//! to guarantee that any postscript file terminates, so denial of service 
//! attacks are absolutely possible on any program that opens them -- and that's
//! the least of the nastyness that could be lurking in there.
//!
use std::{env, str, fmt};
use std::fmt::Debug;
use std::fs::File;
use std::rc::{Rc, Weak};
use std::cell::RefCell;
use std::io::{Read, BufReader, BufRead, Error};
use std::collections::HashMap;

#[derive(Debug, Clone)]
enum EpsValue {
    Name(String),
    Num(f64),
    String(Rc<RefCell<String>>),
    Procedure(EpsProcedure),
    Dict(Rc<RefCell<EpsDict>>),
    Bool(bool),
    Array(Rc<RefCell<Vec<EpsValue>>>),
    Font(f64),
    Save(Rc<EpsVm>),
    Mark,
    Null,
}

#[derive(Debug, Clone)]
pub struct EpsDict {
    vals: HashMap<String, EpsValue>,
    size: usize,
    global: bool,
}

#[derive(Clone)]
pub enum EpsProcedure {
    Builtin(&'static dyn Fn(&mut EpsState), String),
    User(String, u64),
    Nop(u64),
}

#[derive(Debug)]
pub struct EpsState<'a> {
    vm: EpsVm,
    parser_state: ParserState,
    graphics_backend: &'a mut dyn GraphicsBackend,
}

#[derive(Clone)]
pub struct EpsVm {
    status_dict: Rc<RefCell<EpsDict>>,
    user_dict: Rc<RefCell<EpsDict>>,
    global_dict: Rc<RefCell<EpsDict>>,
    stack: Vec<EpsValue>,
    dict_stack: Vec<Rc<RefCell<EpsDict>>>,
}

include!("system_procedures.rs");
include!("buf_read_until.rs");
include!("parser.rs");
include!("plotter.rs");
include!("backend_stateful.rs");

fn wrap_rccell<T>(w: T)-> Rc<RefCell<T>> {
    Rc::new(RefCell::new(w))
}

fn main() -> Result<(), Error> {
    let args: Vec<String> = env::args().collect();

    let filename = &args[1];
    println!("Opening EPS file {}", filename);
    let file = File::open(filename)?;
    let buf = BufReader::new(file);
    // Initialize graphics backend:
    let mut plotter = StdoutPlotter{};
    let mut gbackend = StatefulBackend::new(&mut plotter);
    // Initialize EPS state:
    let mut state = EpsState {
        vm: EpsVm {
            stack: vec![],
            dict_stack: vec![],
            status_dict: wrap_rccell(EpsDict {
                vals: HashMap::new(),
                global: false,
                size: 0
            }),
            user_dict: wrap_rccell(EpsDict {
                vals: HashMap::new(),
                global: false,
                size: 0
            }),
            global_dict: wrap_rccell(EpsDict {
                vals: HashMap::new(),
                global: true,
                size: 0
            }),
        },
        parser_state: ParserState {
            mode: ParserMode::Boundary,
            line_num: 1,
            context: filename.to_string(),
        },
        graphics_backend: &mut gbackend,
    };
    {
        let vm = &mut state.vm;
        let mut t = get_system_dict();
        // Inject dict psuedo-operators
        t.insert("globaldict".into(), EpsValue::Dict(
                Rc::clone(&vm.global_dict)));
        let size = t.len();
        vm.dict_stack.push(wrap_rccell(EpsDict {
            vals: t,
            size: size,
            global: true,
        }));
        vm.dict_stack.push(Rc::clone(&vm.status_dict));
        vm.dict_stack.push(Rc::clone(&vm.global_dict));
        vm.dict_stack.push(Rc::clone(&vm.user_dict));
    }
    eps_run(buf, &mut state, true)
}

fn eps_run<R: Read>(mut buf: BufReader<R>, state: &mut EpsState, exec: bool) 
-> Result<(), Error> {
    let mut tokvec = vec![];
    // Start grinding through the file
    'tokenloop: loop {
        tokvec.clear();
        let tstop =
            buf_read_until(&mut state.parser_state, &mut buf, 
                           &parser, &mut tokvec)?;
        if tstop==None {
            match state.parser_state.mode {
                ParserMode::Token => {
                    state.parser_state.mode = ParserMode::Boundary;
                },
                ParserMode::Boundary => {
                        break;
                },
                _ => {
                    panic!("File ended while scanning {:?}.\n\n{:?}",
                           state.parser_state.mode, state);
                }
            }
        }
        let tokstr = match str::from_utf8(&tokvec) {
            Ok(s) => s.trim(),
            Err(_e) => panic!("Non-UTF token name???"),
        };
        //println!("Got token: {}\nStack: {:?}", tokstr,  state.stack);

        if tokstr.len() == 0 {
            continue; // indentation *shrug*
        }
        if tokstr.starts_with("%") {
            continue;
        }
        if tokstr.starts_with("/") {
            state.vm.stack.push(EpsValue::Name(tokstr[1..].to_string()));
            continue;
        }
        if tokstr.starts_with("(") {
            state.vm.stack.push(EpsValue::String(wrap_rccell(
                    tokstr[1..tokstr.len()-1].to_string()
                    )));
            continue;
        }
        if tokstr.starts_with("{") {
            if tokstr.len() == 1 {
                state.vm.stack.push(EpsValue::Procedure(
                        EpsProcedure::User("".to_string(),
                        state.parser_state.line_num
                        )));
                continue; // empty functions cause problems with the range operator.
            }
            state.vm.stack.push(EpsValue::Procedure(
                    EpsProcedure::User(tokstr[1..tokstr.len()-1].to_string(),
                    state.parser_state.line_num)));
            continue;
        }
        if let Ok(i) = tokstr.parse() {
            state.vm.stack.push(EpsValue::Num(i));
            continue;
        }
        // Executable token resolution:
        let mut fproc = None; // working around borrowing rules
        for dict in state.vm.dict_stack.iter().rev() {
            if let Some(v) = (*dict).borrow().vals.get(tokstr) {
                match v {
                    EpsValue::Procedure(proc) => {
                        if !exec {
                            panic!(
                                "Found executing invocation in {}\n{:?}",
                                "non-executable context!", state);
                        }
                        fproc = Some(proc.clone());
                        break;
                    },
                    _ => {
                        state.vm.stack.push(v.clone());
                        continue 'tokenloop;
                    },
                }
            }
        }
        if let Some(proc) = fproc {
            execute_procedure(proc, state);
            continue 'tokenloop;
        }
        panic!("{}\nTOKEN: '{}'\nCurrent state:\n{:?}",
               "UNIMPLEMENTED TOKEN TYPE!",
               tokstr,
               state);
    }
    Ok(())
}

fn execute_procedure(proc: EpsProcedure, state: &mut EpsState) {
    let procname = format!("In {:?} called from {}:{}", proc, 
                           state.parser_state.context, 
                           state.parser_state.line_num)
        .to_string();
    if let EpsProcedure::Builtin(func, _)=proc {
        (*func)(state);
    } else if let EpsProcedure::User(s, start_line)=proc {
        let buf = BufReader::new(s.as_bytes());
        let mut tpstate = ParserState {
            mode: ParserMode::Boundary,
            line_num: start_line,
            context: procname,
        };
        std::mem::swap(&mut tpstate, &mut state.parser_state);
        eps_run(buf, state, true).expect("IO Error on internal string??");
        std::mem::swap(&mut tpstate, &mut state.parser_state);
    }
}

/// Ignores (very weird, not sure if technically allowed by spec) possibility
/// of putting an operator in an array literal.
fn bind_procedure(proc: EpsProcedure, state: &mut EpsState, out: &mut Vec<String>) {
    let procname = format!("Binding in {:?} {}", proc, state.parser_state.context)
        .to_string();
    match proc {
        EpsProcedure::Builtin(_, name) => {
            out.push(name);
        },
        EpsProcedure::User(s, start_line) =>  {
            let mut buf = BufReader::new(s.as_bytes());
            let mut tpstate = ParserState {
                mode: ParserMode::Boundary,
                line_num: start_line,
                context: procname,
            };
            std::mem::swap(&mut tpstate, &mut state.parser_state);
            let mut tokvec = vec![];
            'tokenloop: loop {
                tokvec.clear();
                let tstop =
                    buf_read_until(&mut state.parser_state, &mut buf, 
                                   &parser, &mut tokvec)
                    .expect("Bad read in binding operation?!?");
                if tstop==None {
                    match state.parser_state.mode {
                        ParserMode::Token => {
                            state.parser_state.mode = ParserMode::Boundary;
                        },
                        ParserMode::Boundary => {
                                break;
                        },
                        _ => {
                            panic!("File ended while binding {:?}.\n\n{:?}",
                                   state.parser_state.mode, state);
                        }
                    }
                }
                let tokstr = match str::from_utf8(&tokvec) {
                    Ok(s) => s.trim(),
                    Err(_e) => panic!("Non-UTF token name???"),
                };
                if tokstr.len() == 0
                    || tokstr.starts_with("%")
                    || tokstr.starts_with("/")
                    || tokstr.starts_with("/")
                    || tokstr.starts_with("(")
                    || tokstr.starts_with("[")
                    || tokstr.starts_with("]")
                    || tokstr.starts_with("{") 
                    || tokstr.parse::<f64>().is_ok() {
                        out.push(tokstr.to_string());
                        continue;
                }
                // Executable token resolution:
                // !!!TODO!!! will infinite loop if is recursive. oh no!
                let mut fproc = None; // working around borrowing rules
                for dict in state.vm.dict_stack.iter().rev() {
                    if let Some(v) = (*dict).borrow().vals.get(tokstr) {
                        match v {
                            EpsValue::Procedure(proc) => {
                                fproc = Some(proc.clone());
                                break;
                            },
                            _ => {
                                out.push(tokstr.to_string());
                                continue 'tokenloop;
                            },
                        }
                    }
                }
                if let Some(proc) = fproc {
                    bind_procedure(proc, state, out);
                    continue 'tokenloop;
                }
                // Permitting unrecognized tokens to remain unbound
                // is apparently how you smuggle global state into
                // bound procedures
                out.push(tokstr.to_string());
            }
            std::mem::swap(&mut tpstate, &mut state.parser_state);
         },
         EpsProcedure::Nop(_) => {},
    }
}

impl fmt::Debug for EpsProcedure {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self {
            EpsProcedure::Builtin(func, name) => {
                write!(f, "Builtin {} at: {:p}", name, func)
            }
            EpsProcedure::User(_s, start_line) => {
                write!(f, "User function starting line {}", start_line)
            }
            EpsProcedure::Nop(start_line) => {
                write!(f, "No-op function starting {}", start_line)
            }
        }
    }
}

/// Indicates that this structure can be cloned in such a manner that the
/// Rcs used in it still have the same 'pools' but now refer to a different
/// chunk of memory; This is a deep-copy that preserves internal references.
trait BreakRcClone {
    /// Deep copy that preserves internal references
    ///
    /// Must have the following properties:
    ///
    ///   - If a: Rc<_> and b: Rc<_> refer to the same value as each other in 
    ///     the original structure, their clones must also refer to the same 
    ///     value as each other (but not the original copy's value).
    ///   - There are no references from the new, cloned struct to the old
    ///     struct's fields (or components of those fields).
    fn break_rc_clone(&self) -> Self;
}

impl BreakRcClone for EpsVm {
    fn break_rc_clone(&self) -> Self {
        let mut str_table = Vec::new();
        let mut dict_table = Vec::new();
        let mut arr_table = Vec::new();
        let mut nstack = Vec::new();
        let mut ndict_stack = Vec::new();
        for e in self.stack.iter().rev() {
            nstack.push(e.break_rc_clone(
                    &mut str_table,
                    &mut dict_table,
                    &mut arr_table));
        }
        for d in self.dict_stack.iter().rev() {
            let d_v = EpsValue::Dict(Rc::clone(d));
            let d_nv = d_v.break_rc_clone(
                        &mut str_table,
                        &mut dict_table,
                        &mut arr_table);
                let final_d = match d_nv {
                EpsValue::Dict(f) => f,
                _ => unreachable!(),
            };
            ndict_stack.push(final_d); // for now
        }
        let status_dict_v = EpsValue::Dict(Rc::clone(&self.status_dict));
        let status_dict_nv = status_dict_v.break_rc_clone(
                    &mut str_table,
                    &mut dict_table,
                    &mut arr_table);
        let status_dict = match status_dict_nv {
            EpsValue::Dict(f) => f,
            _ => unreachable!(),
        };
        let user_dict_v = EpsValue::Dict(Rc::clone(&self.user_dict));
        let user_dict_nv = user_dict_v.break_rc_clone(
                    &mut str_table,
                    &mut dict_table,
                    &mut arr_table);
        let user_dict = match user_dict_nv {
            EpsValue::Dict(f) => f,
            _ => unreachable!(),
        };
        EpsVm {
            status_dict: status_dict,
            user_dict: user_dict,
            stack: nstack,
            dict_stack: ndict_stack,
            global_dict: Rc::clone(&self.global_dict),
        }
    }
}


type ArrayCell = RefCell<Vec<EpsValue>>;
impl EpsValue {
    // ok, implementation thoughts:
    //
    // Can make this take a Weak -> Rc look up table? then
    // just convert old Rcs to weak, then if it exists in table,
    // clone the new pointer and insert it. Otherwise, clone the
    // underlying value and insert a new Rc in the table. At some
    // point, the table gets dropped and the Rcs to the new field
    // values are all that's left.
    fn break_rc_clone(
        &self, 
        str_table: &mut Vec<(Weak<RefCell<String>>, Rc<RefCell<String>>)>,
        dict_table: &mut Vec<(Weak<RefCell<EpsDict>>, Rc<RefCell<EpsDict>>)>,
        arr_table: &mut Vec<(Weak<ArrayCell>, Rc<ArrayCell>)>
        ) -> Self {
        use EpsValue::*;
        match self {
            Name(_) | Num(_) | Procedure(_) | Bool(_) |  Font(_) |  Mark |
                Null => self.clone(),
            Save(ptr) => Save(Rc::clone(ptr)),
            String(ptr) => {
                let w = Rc::downgrade(ptr);
                for tup in str_table.iter() {
                    if Weak::ptr_eq(&w, &tup.0) {
                        return String(Rc::clone(&tup.1));
                    }
                }
                // is clone of underlying refcell & value:
                let nval = (**ptr).clone();
                let nrc = Rc::new(nval);
                str_table.push((w, Rc::clone(&nrc)));
                String(nrc)
            },
            Dict(dict) => {
                if (*dict).borrow().global {
                    Dict(Rc::clone(dict))
                } else {
                    let w = Rc::downgrade(dict);
                    for tup in dict_table.iter() {
                        if Weak::ptr_eq(&w, &tup.0) {
                            return Dict(Rc::clone(&tup.1));
                        }
                    }
                    let nval = RefCell::new(
                            (*dict).borrow()
                            .break_rc_clone(str_table, dict_table, arr_table)
                            );
                    let nrc = Rc::new(nval);
                    dict_table.push((w, Rc::clone(&nrc)));
                    Dict(nrc)
                }
            },
            Array(arr) => {
                let w = Rc::downgrade(arr);
                for tup in arr_table.iter() {
                    if Weak::ptr_eq(&w, &tup.0) {
                        return Array(Rc::clone(&tup.1));
                    }
                }
                let nrc = 
                    break_rc_clone_array(
                        arr, 
                        str_table, 
                        dict_table, 
                        arr_table);
                arr_table.push((w, Rc::clone(&nrc)));
                Array(nrc)
            },
        }
    }
}

impl EpsDict {
    fn break_rc_clone(
        &self, 
        str_table: &mut Vec<(Weak<RefCell<String>>, Rc<RefCell<String>>)>,
        dict_table: &mut Vec<(Weak<RefCell<EpsDict>>, Rc<RefCell<EpsDict>>)>,
        arr_table: &mut Vec<(Weak<ArrayCell>, Rc<ArrayCell>)>
        ) -> Self {
        if self.global {
            unreachable!("Do not attempt to clone global dicts!");
        }
        let mut nvals = HashMap::new();
        for (name, val) in self.vals.iter() {
            nvals.insert(name.clone(), 
                         val.break_rc_clone(str_table, dict_table, arr_table)
                         );
        }
        EpsDict {
            vals: nvals,
            size: self.size,
            global: self.global,
        }
    }
}

fn break_rc_clone_array(
    arr: &Rc<ArrayCell>, 
    str_table: &mut Vec<(Weak<RefCell<String>>, Rc<RefCell<String>>)>,
    dict_table: &mut Vec<(Weak<RefCell<EpsDict>>, Rc<RefCell<EpsDict>>)>,
    arr_table: &mut Vec<(Weak<ArrayCell>, Rc<ArrayCell>)>
    ) -> Rc<ArrayCell> {
    let mut res = vec![];
    for el in (**arr).borrow().iter() {
        res.push(el.break_rc_clone(str_table, dict_table, arr_table));
    }
    Rc::new(RefCell::new(res))
}

impl fmt::Debug for EpsVm { //Because the autoderived impl is unnecessarily verbose
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "STACK: ")?;
        self.stack.fmt(f)?;
        write!(f, "\nDICTIONARY STACK: ")?;
        self.dict_stack.fmt(f)?;
        write!(f, "\n")
    }
}

impl Eq for EpsValue {} // stub impl for marker
impl PartialEq for EpsValue {
    fn eq(&self, other: &Self) -> bool {
        use crate::EpsValue::*;
        match (self, other) {
            (Name(s), Name(d)) => s == d,
            (Num(s), Num(d)) => s == d,
            (String(s), String(d)) => Rc::ptr_eq(s, d),
            (Procedure(s), Procedure(d)) => s == d,
            (Dict(s), Dict(d)) => Rc::ptr_eq(s, d),
            (Bool(s), Bool(d)) => s == d,
            (Array(s), Array(d)) => Rc::ptr_eq(s, d),
            (Font(s), Font(d)) => s == d, // switch font size to graphic state?
            (Save(_s), Save(_d)) => false, // meh
            (Mark, Mark) => true,
            (Null, Null) => true, // I think.
            _ => false,
        }
    }
}

impl Eq for EpsProcedure {} // stub impl for marker
impl PartialEq for EpsProcedure {
    fn eq(&self, other: &Self) -> bool {
        use crate::EpsProcedure::*;
        match (self, other) {
            (Builtin(_, s), Builtin(_, d)) => s == d, 
            (User(s, a), User(d, b)) => s == d && a == b,
            (Nop(_), Nop(_)) => true,
            _ => false,
        }
    }
}
