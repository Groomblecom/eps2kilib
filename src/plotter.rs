#[derive(Debug, Clone)]
enum PathSegment {
    Line((f64, f64), (f64, f64)),
}

trait Plotter: Debug {
    fn closed_shape(&mut self, parts: &Vec<PathSegment>);
    fn open_path(&mut self, parts: &Vec<PathSegment>);
    fn clip_path(&mut self, parts: &Vec<PathSegment>);
    fn page(&mut self);
    fn set_page_size(&mut self, w: f64, h: f64);
}

#[derive(Debug)]
struct StdoutPlotter {}

impl Plotter for StdoutPlotter {
    fn closed_shape(&mut self, parts: &Vec<PathSegment>) {
        println!("Closed shape: {:?}", parts);
    }
    fn open_path(&mut self, parts: &Vec<PathSegment>) {
        println!("Open path: {:?}", parts);
    }
    fn clip_path(&mut self, parts: &Vec<PathSegment>) {
        println!("new clipping path: {:?}", parts);
    }
    fn page(&mut self) {
        println!("PAGE PAGE PAGE PAGE PAGE PAGE PAGE PAGE");
    }
    fn set_page_size(&mut self, w: f64, h: f64) {
        println!("Setting page size to {} wide by {} high", w, h);
    }
}
